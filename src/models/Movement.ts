export type Movement = {
  id: number | null;
  date: string;
  amount: number;
  type: string;
  categoryId: number;
  subCategoryId: number | null;
  notes: string;
};
