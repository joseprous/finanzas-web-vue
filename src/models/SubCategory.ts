export type SubCategory = {
  id: number;
  name: string;
};
