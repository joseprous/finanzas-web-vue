import { SubCategory } from './SubCategory';

export type Category = {
  id: number;
  name: string;
  subCategories: Array<SubCategory>;
  type: string;
};
