import { Movement } from './Movement';

export type GetMovementsResponse = {
  total: number;
  movements: Array<Movement>;
};
