import { Category } from 'src/models/Category';
import { Movement } from 'src/models/Movement';
import { GetMovementsResponse } from 'src/models/GetMovementsResponse';
import axios from 'axios';

class MovementService {
  async getCategories(type?: number): Promise<Array<Category>> {
    let query = '';
    if (type != null) {
      query += `Type=${type}&`;
    }
    const response = await axios.get<Array<Category>>(
      `${process.env.API_URL}/api/categories?${query}`
    );
    return response.data;
  }

  async getMovements(startMonth, endMonth): Promise<GetMovementsResponse> {
    let query = '';
    query += `StartMonth=${startMonth}&`;
    query += `EndMonth=${endMonth}&`;
    const response = await axios.get<GetMovementsResponse>(
      `${process.env.API_URL}/api/movements?${query}`
    );
    return response.data;
  }

  async getPlainMovements(startMonth, endMonth) {
    let query = '';
    query += `StartMonth=${startMonth}&`;
    query += `EndMonth=${endMonth}&`;
    const response = await axios.get(
      `${process.env.API_URL}/api/movements/plain?${query}`
    );
    return response.data;
  }

  async createMovement(movement: Movement): Promise<number> {
    const response = await axios.post<number>(
      `${process.env.API_URL}/api/movements`,
      movement
    );
    return response.data;
  }
}

export default new MovementService();
